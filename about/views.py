from django.shortcuts import render

# Create your views here.
def index(request):
    return render(request,'about/index.html')

def socmed(request):
    return render(request,'about/socmed.html')